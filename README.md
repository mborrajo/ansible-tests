# Tests de Ansible
### Comandos Ad-hoc
`ansible all -i hosts -m shell -a "ls -l"`


### Lanzar el playbook
`ansible-playbook -i hosts site.yml`

### Sacar todas las variables:
`ansible monitoring_server -i hosts -m setup > allvars.json` 

### SSH

#### Generar claves
`ssh-keygen -t rsa`

#### Copiar claves
`ssh-copy-id root@198.168.1.XXX`
